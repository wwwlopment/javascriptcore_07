function Robot(text) {
  this.text = text;
}

Robot.prototype.work = function () {
  console.log('Я ' + this.constructor.name + ' – ' + this.text);
}

function CoffeeRobot(text) {
  this.text = text;
}

CoffeeRobot.prototype = Object.create(Robot.prototype);
CoffeeRobot.prototype.constructor = CoffeeRobot;


function RobotDancer(text) {
  this.text = text;
}

RobotDancer.prototype = Object.create(Robot.prototype);
RobotDancer.prototype.constructor = RobotDancer;


function RobotCoocker(text) {
  this.text = text;
}

RobotCoocker.prototype = Object.create(Robot.prototype);
RobotCoocker.prototype.constructor = RobotCoocker;




var robot = new Robot('я просто працюю');
var coffeeRobot = new CoffeeRobot('я варю каву');
var robotDancer = new RobotDancer('я просто танцюю');
var robotCoocker = new RobotCoocker('я просто готую');

var robots = new Array(robot, coffeeRobot, robotDancer, robotCoocker);

for (var i = 0; i < robots.length; i++) {
  robots[i].work();
}

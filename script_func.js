function Robot() {
  this.text = 'я просто працюю';
  this.work = function () {
    console.log('Я ' + this.constructor.name + ' – ' + this.text);
  };
}

function CoffeeRobot() {
  Robot.call(this);
  this.text = 'я варю каву';
}

function RobotDancer() {
  Robot.call(this);
  this.text = 'я просто танцюю';
}


function RobotCoocker() {
  Robot.call(this);
  this.text = 'я просто готую';
}

var robot = new Robot();
var coffeeRobot = new CoffeeRobot();
var robotDancer = new RobotDancer();
var robotCoocker = new RobotCoocker();

var robots = new Array(robot, coffeeRobot, robotDancer, robotCoocker);

for (var i = 0; i < robots.length; i++) {
  robots[i].work();
}
